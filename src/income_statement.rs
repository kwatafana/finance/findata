use base64::prelude::*;
use chrono::NaiveDate;
use kommon::{ID, VERSION};
use serde::{de::Deserializer, Deserialize, Serialize};

/// Income Statement, raw as we get it upstream
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct IncomeStatementInput {
    pub symbol: Option<String>,
    pub fiscal_date_ending: NaiveDate,
    pub reported_currency: String,
    #[serde(deserialize_with = "deserialize")]
    pub gross_profit: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub total_revenue: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub cost_of_revenue: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub costof_goods_and_services_sold: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub operating_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub selling_general_and_administrative: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub research_and_development: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub operating_expenses: Option<i64>,
    pub investment_income_net: String,
    #[serde(deserialize_with = "deserialize")]
    pub net_interest_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub interest_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub interest_expense: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub non_interest_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub other_non_operating_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub depreciation: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub depreciation_and_amortization: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub income_before_tax: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub income_tax_expense: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub interest_and_debt_expense: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub net_income_from_continuing_operations: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub comprehensive_income_net_of_tax: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub ebitda: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub net_income: Option<i64>,
}

/// Income Statement
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct IncomeStatement {
    pub id: ID,
    pub symbol: Option<String>,
    pub fiscal_date_ending: NaiveDate,
    pub reported_currency: String,
    #[serde(deserialize_with = "deserialize")]
    pub gross_profit: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub total_revenue: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub cost_of_revenue: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub costof_goods_and_services_sold: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub operating_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub selling_general_and_administrative: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub research_and_development: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub operating_expenses: Option<i64>,
    pub investment_income_net: String,
    #[serde(deserialize_with = "deserialize")]
    pub net_interest_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub interest_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub interest_expense: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub non_interest_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub other_non_operating_income: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub depreciation: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub depreciation_and_amortization: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub income_before_tax: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub income_tax_expense: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub interest_and_debt_expense: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub net_income_from_continuing_operations: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub comprehensive_income_net_of_tax: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub ebitda: Option<i64>,
    #[serde(deserialize_with = "deserialize")]
    pub net_income: Option<i64>,
    /// Date income statement was filed
    pub filed: NaiveDate,
    /// Version of schema
    pub version: VERSION,
}

/// Deserialize
fn deserialize<'de, D>(deserializer: D) -> Result<Option<i64>, D::Error>
where
    D: Deserializer<'de>,
{
    let buf = String::deserialize(deserializer)?;

    if buf == "None".to_string() {
        return Ok(None);
    } else {
        let val: i64 = buf.trim().parse().map_err(serde::de::Error::custom)?;
        Ok(Some(val))
    }
}

pub struct IncomeStatementDatabaseInput {
    pub symbol: Option<String>,
    pub fiscal_date_ending: NaiveDate,
    pub reported_currency: String,
    pub gross_profit: Option<i64>,
    pub total_revenue: Option<i64>,
    pub cost_of_revenue: Option<i64>,
    pub costof_goods_and_services_sold: Option<i64>,
    pub operating_income: Option<i64>,
    pub selling_general_and_administrative: Option<i64>,
    pub research_and_development: Option<i64>,
    pub operating_expenses: Option<i64>,
    pub investment_income_net: String,
    pub net_interest_income: Option<i64>,
    pub interest_income: Option<i64>,
    pub interest_expense: Option<i64>,
    pub non_interest_income: Option<i64>,
    pub other_non_operating_income: Option<i64>,
    pub depreciation: Option<i64>,
    pub depreciation_and_amortization: Option<i64>,
    pub income_before_tax: Option<i64>,
    pub income_tax_expense: Option<i64>,
    pub interest_and_debt_expense: Option<i64>,
    pub net_income_from_continuing_operations: Option<i64>,
    pub comprehensive_income_net_of_tax: Option<i64>,
    pub ebitda: Option<i64>,
    pub net_income: Option<i64>,
    pub filed: NaiveDate,
    pub version: VERSION,
}

impl IncomeStatementDatabaseInput {
    pub fn hash(&self) -> String {
        let mut hasher = blake3::Hasher::new();
        hasher.update(format!("{}", &self.fiscal_date_ending).as_bytes());
        hasher.update(&self.reported_currency.as_bytes());

        if let Some(symbol) = &self.symbol {
            hasher.update(symbol.as_bytes());
        }

        if let Some(gross_profit) = self.gross_profit {
            hasher.update(format!("{gross_profit}").as_bytes());
        }

        if let Some(total_revenue) = self.total_revenue {
            hasher.update(format!("{total_revenue}").as_bytes());
        }

        if let Some(cost_of_revenue) = self.cost_of_revenue {
            hasher.update(format!("{cost_of_revenue}").as_bytes());
        }

        if let Some(costof_goods_and_services_sold) = self.costof_goods_and_services_sold {
            hasher.update(format!("{costof_goods_and_services_sold}").as_bytes());
        }

        if let Some(operating_income) = self.operating_income {
            hasher.update(format!("{operating_income}").as_bytes());
        }

        if let Some(selling_general_and_administrative) = self.selling_general_and_administrative {
            hasher.update(format!("{selling_general_and_administrative}").as_bytes());
        }

        if let Some(research_and_development) = self.research_and_development {
            hasher.update(format!("{research_and_development}").as_bytes());
        }

        if let Some(operating_expenses) = self.operating_expenses {
            hasher.update(format!("{operating_expenses}").as_bytes());
        }

        hasher.update(&self.investment_income_net.as_bytes());

        if let Some(net_interest_income) = self.net_interest_income {
            hasher.update(format!("{net_interest_income}").as_bytes());
        }

        if let Some(interest_income) = self.interest_income {
            hasher.update(format!("{interest_income}").as_bytes());
        }

        if let Some(interest_expense) = self.interest_expense {
            hasher.update(format!("{interest_expense}").as_bytes());
        }

        if let Some(non_interest_income) = self.non_interest_income {
            hasher.update(format!("{non_interest_income}").as_bytes());
        }

        if let Some(other_non_operating_income) = self.other_non_operating_income {
            hasher.update(format!("{other_non_operating_income}").as_bytes());
        }

        if let Some(depreciation) = self.depreciation {
            hasher.update(format!("{depreciation}").as_bytes());
        }

        if let Some(depreciation_and_amortization) = self.depreciation_and_amortization {
            hasher.update(format!("{depreciation_and_amortization}").as_bytes());
        }

        if let Some(income_before_tax) = self.income_before_tax {
            hasher.update(format!("{income_before_tax}").as_bytes());
        }

        if let Some(income_tax_expense) = self.income_before_tax {
            hasher.update(format!("{income_tax_expense}").as_bytes());
        }

        if let Some(interest_and_debt_expense) = self.interest_and_debt_expense {
            hasher.update(format!("{interest_and_debt_expense}").as_bytes());
        }

        if let Some(net_income_from_continuing_operations) =
            self.net_income_from_continuing_operations
        {
            hasher.update(format!("{net_income_from_continuing_operations}").as_bytes());
        }

        if let Some(comprehensive_income_net_of_tax) = self.comprehensive_income_net_of_tax {
            hasher.update(format!("{comprehensive_income_net_of_tax}").as_bytes());
        }

        if let Some(ebitda) = self.ebitda {
            hasher.update(format!("{ebitda}").as_bytes());
        }

        if let Some(net_income) = self.net_income {
            hasher.update(format!("{net_income}").as_bytes());
        }

        let hash = hasher.finalize();
        BASE64_STANDARD.encode(hash.as_bytes())
    }
}
